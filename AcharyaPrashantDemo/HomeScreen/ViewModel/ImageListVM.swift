//
//  ImageListVM.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 15/04/24.
//

import Foundation

class ImageListVM : NSObject{
    
    var success  : (([ImageCodable])-> ())? = nil
    var errorStr : ((String)-> Void)? = nil
    var API      : WebService!
    var page     = 1
    
    override init() {
        super.init()
        self.API = WebService()
        self.getImageList()
    }
    
    func getImageList() {
        API.callWebAPI(url:kBaseURL+"page=\(self.page)") { data in
            let imageData = try! JSONDecoder().decode([ImageCodable].self, from: data)
            self.success!(imageData)
        } failure: { errorMesg in
            print("error===\(errorMesg)")
            self.errorStr?(errorMesg)
        }
    }
}

