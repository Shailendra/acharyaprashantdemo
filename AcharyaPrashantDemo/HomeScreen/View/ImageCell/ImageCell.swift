//
//  ImageCell.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 15/04/24.
//

import UIKit

class ImageCell: UICollectionViewCell {

    @IBOutlet var imageView : ImageView!
    class var identifier : String {return String(describing: self)}
    class var nib : UINib{return UINib(nibName: self.identifier, bundle: nil)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView.layer.cornerRadius = 8
        self.imageView.layer.borderColor  = UIColor.gray.cgColor
        self.imageView.layer.borderWidth  = 2.0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //self.imageView.image = nil
    }
}

