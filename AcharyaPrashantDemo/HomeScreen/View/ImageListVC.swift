//
//  ViewController.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 14/04/24.
//

import UIKit


class ViewController: UIViewController {
    
    let fullSize        = UIScreen.main.bounds
    @IBOutlet weak var imageCollectionView : UICollectionView!
    lazy var viewModel = ImageListVM()
    var imageData      = [ImageCodable]()
    
    override func viewDidLoad() {
        
        self.viewModel.success = { (imageData) in
            self.imageData.append(contentsOf: imageData)
            DispatchQueue.main.async {
                self.imageCollectionView.reloadData()
            }
        }
        self.viewModel.errorStr = { (message) in
            DispatchQueue.main.async {
                self.presentAlert(withTitle: kAlert, message: message, actions: [.kOK : .default] , completionHandler: nil)
            }
        }
        self.initCollectionView()
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    internal func initCollectionView(){
        self.imageCollectionView.register(ImageCell.nib, forCellWithReuseIdentifier: ImageCell.identifier)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: self.fullSize.width/2, height: self.fullSize.width/2)
        self.imageCollectionView!.collectionViewLayout = layout
    }
}


