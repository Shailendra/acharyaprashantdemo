//
//  ImageCollectionView.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 15/04/24.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.identifier, for: indexPath) as? ImageCell else { return UICollectionViewCell() }
        
        DispatchQueue.main.async {
            cell.imageView.loadImages(from: self.imageData[indexPath.row].urls!.smallUrl)
        }
        return cell
    }
}

extension ViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.viewModel.page += 1
        self.viewModel.getImageList()
    }
}
