//
//  ImageListM.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 15/04/24.
//

import Foundation

struct ImageCodable : Codable {
    
    let urls : Urls?
    
    enum CodingKeys: String, CodingKey {
        case urls = "urls"
    }
   
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.urls = try values.decodeIfPresent(Urls.self, forKey: .urls)
    }
}

struct Urls: Codable {
    let small: String
    
    var smallUrl: URL {
        return URL(string: self.small)!
    }
}
