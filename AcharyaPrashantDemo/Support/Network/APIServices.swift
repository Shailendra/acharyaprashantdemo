//
//  APIServices.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 14/04/24.
//

import UIKit

class WebService{

  
    func callWebAPI(url : String, method : HTTPMETHOD = .GET ,params : [String:Any]? = nil, success :@escaping ((Data) -> Void), failure :@escaping ((String) -> Void)){
        if(Reachability.isConnectedToNetwork()){
            guard let url = URL(string: url) else {
                return
            }
            print("Url====\(url)")
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = method.rawValue
            request.setValue(kContentTypeValueKey, forHTTPHeaderField: kContentTypeKey)
            if params != nil{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params!, options: .prettyPrinted)
                    request.httpBody = jsonData
                    request.setValue(String.init(format: "%i", (jsonData.count)), forHTTPHeaderField: kContentLengthKey)
                } catch {
                    print(error.localizedDescription)
                }
            }
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                if data == nil{
                    failure(error.debugDescription)
                }
                else{
                    if  String(data: data! , encoding: .utf8) != nil {
                        success(data!)
                    }
                    else{
                        failure(error.debugDescription)
                    }
                }
            })
            task.resume()
        }else{
            failure(kInternetDiscription)
        }
    }
}
