//
//  Constant.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 14/04/24.
//

import Foundation

//MARK: - API Keys

// Basic
let kContentTypeKey        = "Content-Type"
let kContentTypeValueKey   = "application/json"
let kContentLengthKey      = "Content-Length"


//===========================================
// MARK: - BASE URL
//===========================================

var kBaseURL = "https://api.unsplash.com/photos/?client_id=\(APIKey)&"

