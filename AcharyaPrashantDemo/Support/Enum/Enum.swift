//
//  Enum.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 14/04/24.
//

import Foundation


//===========================================
// MARK:- HTTP METHODS
//===========================================
enum HTTPMETHOD : String {
    case GET  = "GET"
    case POST = "POST"
    case PUT  = "PUT"
    case HEAD = "HEAD"
}

enum Storyboard : String{
    case Main = "Main"
}
