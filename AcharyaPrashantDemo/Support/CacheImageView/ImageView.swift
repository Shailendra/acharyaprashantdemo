//
//  ImageView.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 16/04/24.
//

import Foundation
import UIKit

class ImageView: UIImageView {
    
    static var cache = NSCache<AnyObject, UIImage>()
    var url: URL?
    
    func loadImages(from url: URL) {
        self.url = url
        
        if let cachedImage = ImageView.cache.object(forKey: url as AnyObject) {
            self.image = cachedImage
        }else{
            URLSession.shared.dataTask(with: url) { (data, respnse, error) in
                if let error = error {
                    print("Error: \(error)")
                    ImageView.cache.setObject(UIImage(named: "ic-PlaceHolder")!, forKey: url as AnyObject)
                }else if let data = data {
                    if url == self.url{
                        DispatchQueue.main.async {
                            self.image = UIImage(data: data)
                            ImageView.cache.setObject(self.image!, forKey: url as AnyObject)
                        }
                    }else{
                        ImageView.cache.setObject(UIImage(named: "ic-PlaceHolder")!, forKey: url as AnyObject)
                    }
                }
            }.resume()
        }
    }
}
