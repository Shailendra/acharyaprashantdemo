//
//  Extenstion.swift
//  AcharyaPrashantDemo
//
//  Created by Shailendra on 14/04/24.
//

import Foundation
import UIKit

extension String {
    
    static let kEmpty           = " "
    static let kOK              = "OK"
}

extension UIViewController {
    
    func presentAlert(withTitle title: String, message : String, actions : [String: UIAlertAction.Style], completionHandler: ((UIAlertAction) -> ())? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            
            let action = UIAlertAction(title: action.key, style: action.value) { action in
                
                if completionHandler != nil {
                    completionHandler!(action)
                }
            }
            
            alertController.addAction(action)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}
